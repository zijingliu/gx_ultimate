﻿using UnityEngine;
using System.Collections;

public class EnemyMovementBoss: MonoBehaviour {

	public GameObject Enmemy1;
	public GameObject Enmemy2;
	public GameObject Position1;
	public float FireSpeed;
	
	float NextFire = 0.0f;

	void Update()
	{
		Vector3 movement = new Vector3(2.0f * Mathf.Sin(Time.time), 0f, 0f);
		GetComponent<Rigidbody2D>().transform.position += movement * Time.deltaTime;

		if (Time.time > NextFire) 
		{
			NextFire = Time.time + (1 / FireSpeed);
			Instantiate (Enmemy1, Position1.transform.position, Position1.transform.rotation);
		}
	}

}
