﻿using UnityEngine;
using System.Collections;

public class EnemyMovementSc4 : MonoBehaviour {

	public GameObject bullet;
	public float FireSpeed;
	float NextFire = 0.0f;

	void Update()
	{
		if (Time.time > NextFire) 
		{
			NextFire = Time.time + (1 / FireSpeed);
			Instantiate (bullet, this.transform.position, this.transform.rotation);
		}
	}
	
}
