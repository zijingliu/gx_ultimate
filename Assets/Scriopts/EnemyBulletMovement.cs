﻿using UnityEngine;
using System.Collections;

public class EnemyBulletMovement : MonoBehaviour {

	public float BulletSpeed;

	void Start() 
	{
		GetComponent<Rigidbody2D>().velocity = new Vector2 (0.0f, -BulletSpeed);
	}
}
