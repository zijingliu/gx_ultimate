﻿using UnityEngine;
using System.Collections;

public class BossContact : MonoBehaviour
{
	public GameObject Explosion;
	public GameObject Explosion_Player;
	public int ScoreValue;

	private GameController gameController;
	private float health = 100f;
	
	void Start ()
	{
		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		gameController = gameControllerObject.GetComponent <GameController>();
	}

	void OnTriggerEnter2D(Collider2D other) 
	{
		if (other.tag == "Boundary") 
		{
			return;
		} 
		if(other.tag == "Player")
		{
			Instantiate(Explosion_Player, other.transform.position, other.transform.rotation);
			gameController.GameOver();
		}

		if(health == 0)
		{
			Instantiate(Explosion, transform.position, transform.rotation);
			Destroy (gameObject);
			gameController.AddScore(ScoreValue);
		}

		health--;
		Destroy (other.gameObject);
	}
}