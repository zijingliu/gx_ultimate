﻿using UnityEngine;
using System.Collections;

public class EnemyMovement: MonoBehaviour {

	public GameObject bullet;
	public GameObject Position;
	public float FireSpeed;
	public float VerticalSpeed;
	
	float NextFire = 0.0f;
	
	void Start()
	{
		Vector2 movement = new Vector2 (0f, -VerticalSpeed);
			GetComponent<Rigidbody2D>().velocity = movement;
	}
	
	void Update()
	{
		if (Time.time > NextFire) 
		{
			NextFire = Time.time + (1 / FireSpeed);
			Instantiate (bullet, Position.transform.position, Position.transform.rotation);
		}
	}

}
