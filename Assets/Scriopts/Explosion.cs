﻿using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour {

	public Texture[] Frames;
	public float FPS;

	private float Timer;

	void Start()
	{
		Timer = Time.time;
	}

	// Update is called once per frame
	void Update () {
		int index = (int)(Time.time * FPS) % Frames.Length;
		GetComponent<Renderer>().material.mainTexture = Frames [index];
		if ((Time.time - Timer) > (Frames.Length / (1.0f * FPS))) 
		{
			Destroy(gameObject);
		}
	}
}