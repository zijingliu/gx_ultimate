﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Boundary2
{
	public float MaxY, MinY, MaxX, MinX;
}
public class Player2Controller : MonoBehaviour
{
	public float Speed;
	public Boundary2 boundary;
	public float FireSpeed;
	public GameObject Bullet;
	public Transform BulletSpawn;
	public Transform BulletSpawn21;
	public Transform BulletSpawn22;
	
	private float NextFire = 0;
	private int power = 0;

	private GameController gameController;
	
	void Start ()
	{
		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		gameController = gameControllerObject.GetComponent <GameController>();
	}
	
	
	void Update()
	{
		if (Time.time > NextFire && Input.GetKey (KeyCode.KeypadEnter)) 
		{
			NextFire = Time.time + (1 / FireSpeed);
			Fire();
		}
	}
	
	void FixedUpdate()
	{
		float MoveHorizontal = Input.GetAxis("Horizontal2");
		float MoveVertical = Input.GetAxis("Vertical2");
		
		Vector2 Movement = new Vector2 (MoveHorizontal, MoveVertical);
		GetComponent<Rigidbody2D>().velocity = Movement * Speed;
		
		GetComponent<Rigidbody2D>().position = new Vector2
		(
			Mathf.Clamp(GetComponent<Rigidbody2D>().position.x, boundary.MinX, boundary.MaxX),
			Mathf.Clamp(GetComponent<Rigidbody2D>().position.y, boundary.MinY, boundary.MaxY)
		);
	}
	
	void Fire()
	{
		if(power == 0)
		{
			FireSpeed = 8;
			Instantiate (Bullet, BulletSpawn.position, BulletSpawn.rotation);
		}
		else if(power > 0 && power <= 500)
		{
			Instantiate (Bullet, BulletSpawn21.position, BulletSpawn21.rotation);
			Instantiate (Bullet, BulletSpawn22.position, BulletSpawn22.rotation);
			power--;
		}
	}
	
	void OnTriggerEnter2D(Collider2D other) 
	{
		if(other.tag == "Exp")
		{
			power += 100;
			FireSpeed = 11;
			Destroy(other.gameObject);
		}
		else if(other.tag == "Bomb")
		{
			Destroy(other.gameObject);
			gameController.addBomb();
		}
	}
}



