﻿using UnityEngine;
using System.Collections;

public class LifeCollide : MonoBehaviour {

	private GameController gameController;
	
	void Start ()
	{
		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		gameController = gameControllerObject.GetComponent <GameController>();
	}
	
	void OnTriggerEnter2D(Collider2D other) 
	{
		if(other.tag == "Player")
		{
			Destroy(gameObject);
			gameController.addLife();
			return;
		}
	}
}
