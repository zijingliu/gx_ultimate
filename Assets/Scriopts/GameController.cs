﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour
{
	public GameObject player;
	public GameObject player2;

	public GameObject Enemy_1;
	public GameObject Enemy_2;
	public GameObject Enemy_3;
	public GameObject Enemy_4;
	public GameObject Enemy_6;
	public GameObject Enemy_Boss;
	public GameObject lifeBox;
	public GameObject ExpBox;
	public GameObject BombBox;
	public GameObject Explosion;

	public Vector3 SpawnValues;
	public float WaitStart;
	public float WaitWaves;
	//public GameObject Respawn;

	public GUIText Score;
	public GUIText GameStart;
	public GUIText GameStart2;
	public GUIText GameStart3;
	public GUIText GameEnd;
	public GUIText GameEnd2;
	public GUIText RespawnText;
	public GUIText lifeNum;
	public GUIText Abomb;

	public int bombNum;

	private int score;
	private bool dead;
	private bool start;
	private bool boss;
	private int life;
	private int Relife;
	private bool muti;
	
	private Vector3 newSpawn;
	


	void Start()
	{
		score = 0;
		life = 1;
		Relife = 0;
		dead = false;
		start = true;
		boss = false;
		muti = false;
		newSpawn = new Vector3(0, 0, 0);
		UpdateScore ();
		updateLife ();
	}

	void Update()
	{
		if(Input.GetKey(KeyCode.Return) && start)
		{
			start = false;
			GameStart.text = "";
			GameStart2.text = "";
			GameStart3.text = "";
			StartCoroutine (SpawnWaves());
		}
		else if(Input.GetKey(KeyCode.T) && start)
		{
			life++;
			muti = true;
			Vector3 playerspawn = new Vector3(5.63f, 0.18f, 0);
			Instantiate(player2, playerspawn, Quaternion.identity);
			start = false;
			GameStart.text = "";
			GameStart2.text = "";
			GameStart3.text = "";
			StartCoroutine (SpawnWaves());
		}

		if(Input.GetKey(KeyCode.P)&& bombNum>0 )
		{
			bombNum--;
			updateBomb();
			DestroyAllGameObjects();
		}

		if(GameObject.Find("Player") == null && GameObject.Find("Player(Clone)") == null && !dead && Relife > 0)
		{
			if(Input.GetKey(KeyCode.R))
			{
				Instantiate(player, newSpawn, Quaternion.identity);
				Relife--;
				life++;
				updateLife();
			}
		}
		else if(GameObject.Find("Player 1(Clone)") == null && !dead && muti && Relife > 0)
		{
			if(Input.GetKey(KeyCode.R))
			{
				newSpawn.x += 2.5f;
				Instantiate(player2, newSpawn, Quaternion.identity);
				newSpawn.x -= 2.5f;
				Relife--;
				life++;
				updateLife();
			}
		}
		else
		{
			RespawnText.text = "";
		}

		if(dead)
		{	
			if(Input.GetKey (KeyCode.O))
			{
				Application.LoadLevel (Application.loadedLevel);
			}

		}
	}
	

	IEnumerator SpawnWaves()
	{
		float wait = 0.5f;
		int counter = 10;
		yield return new WaitForSeconds (WaitStart);
		while(true)
		{
			if(score <= 20)
			{
				for(int i =0; i < 10; i++)
				{
					Vector3 SpawnPosition = new Vector3 (Random.Range (-SpawnValues.x + 3.5f, SpawnValues.x - 3.5f), SpawnValues.y, SpawnValues.z);
					Quaternion SpawnRotation = Quaternion.identity;
					Instantiate (Enemy_3, SpawnPosition, SpawnRotation);
					yield return new WaitForSeconds(0.3f);
					if(dead)
					{
						break;
					}
					if(Random.Range(0, 100f) > 95)
					{
						SpawnPosition = new Vector3 (Random.Range (-SpawnValues.x + 3.5f, SpawnValues.x - 3.5f), SpawnValues.y, SpawnValues.z);
						SpawnRotation = Quaternion.identity;
						Instantiate(lifeBox, SpawnPosition, SpawnRotation);
					}
					if(Random.Range(0, 100f) > 95)
					{
						SpawnPosition = new Vector3 (Random.Range (-SpawnValues.x + 3.5f, SpawnValues.x - 3.5f), SpawnValues.y, SpawnValues.z);
						SpawnRotation = Quaternion.identity;
						Instantiate(ExpBox, SpawnPosition, SpawnRotation);
					}
					if(Random.Range(0, 100f) > 95)
					{
						SpawnPosition = new Vector3 (Random.Range (-SpawnValues.x + 3.5f, SpawnValues.x - 3.5f), SpawnValues.y, SpawnValues.z);
						SpawnRotation = Quaternion.identity;
						Instantiate(BombBox, SpawnPosition, SpawnRotation);
					}
				}
			}
			else if(score > 20 && score < 45)
			{
				for(int i = 0; i < 3; i++)
				{
					Vector3 SpawnPosition = new Vector3 (Random.Range (-SpawnValues.x, SpawnValues.x - 4.5f), SpawnValues.y, SpawnValues.z);
					Quaternion SpawnRotation = Quaternion.identity;
					Instantiate (Enemy_2, SpawnPosition, SpawnRotation);
					SpawnPosition.x += 1.5f;
					Instantiate (Enemy_2, SpawnPosition, SpawnRotation);
					SpawnPosition.x += 1.5f;
					Instantiate (Enemy_2, SpawnPosition, SpawnRotation);
					SpawnPosition.x += 1.5f;
					Instantiate (Enemy_2, SpawnPosition, SpawnRotation);
					yield return new WaitForSeconds(0.8f);
					if(dead)
					{
						break;
					}
					if(Random.Range(0, 100f) > 95)
					{
						SpawnPosition = new Vector3 (Random.Range (-SpawnValues.x + 3.5f, SpawnValues.x - 3.5f), SpawnValues.y, SpawnValues.z);
						SpawnRotation = Quaternion.identity;
						Instantiate(lifeBox, SpawnPosition, SpawnRotation);
					}
					if(Random.Range(0, 100f) > 95)
					{
						SpawnPosition = new Vector3 (Random.Range (-SpawnValues.x + 3.5f, SpawnValues.x - 3.5f), SpawnValues.y, SpawnValues.z);
						SpawnRotation = Quaternion.identity;
						Instantiate(ExpBox, SpawnPosition, SpawnRotation);
					}
				}
			}
			else if(score >= 45 && score < 70)
			{
				for(int i =0; i < 10; i++)
				{
					Vector3 SpawnPosition = new Vector3 (Random.Range (-SpawnValues.x, SpawnValues.x), SpawnValues.y, SpawnValues.z);
					Quaternion SpawnRotation = Quaternion.identity;
					Instantiate (Enemy_1, SpawnPosition, SpawnRotation);
					yield return new WaitForSeconds(0.5f);
					if(dead)
					{
						break;
					}
					if(Random.Range(0, 100f) > 95)
					{
						SpawnPosition = new Vector3 (Random.Range (-SpawnValues.x + 3.5f, SpawnValues.x - 3.5f), SpawnValues.y, SpawnValues.z);
						SpawnRotation = Quaternion.identity;
						Instantiate(lifeBox, SpawnPosition, SpawnRotation);
					}
					if(Random.Range(0, 100f) > 95)
					{
						SpawnPosition = new Vector3 (Random.Range (-SpawnValues.x + 3.5f, SpawnValues.x - 3.5f), SpawnValues.y, SpawnValues.z);
						SpawnRotation = Quaternion.identity;
						Instantiate(ExpBox, SpawnPosition, SpawnRotation);
					}
				}
			}
			else if(score >= 70 && score < 100)
			{
				for(int i =0; i < 10; i++)
				{
					Vector3 SpawnPosition = new Vector3 (Random.Range (-SpawnValues.x, SpawnValues.x), SpawnValues.y, SpawnValues.z);
					Quaternion SpawnRotation = Quaternion.identity;
					Instantiate (Enemy_4, SpawnPosition, SpawnRotation);
					yield return new WaitForSeconds(0.5f);
					if(dead)
					{
						break;
					}
					if(Random.Range(0, 100f) > 95)
					{
						SpawnPosition = new Vector3 (Random.Range (-SpawnValues.x + 3.5f, SpawnValues.x - 3.5f), SpawnValues.y, SpawnValues.z);
						SpawnRotation = Quaternion.identity;
						Instantiate(lifeBox, SpawnPosition, SpawnRotation);
					}
					if(Random.Range(0, 100f) > 95)
					{
						SpawnPosition = new Vector3 (Random.Range (-SpawnValues.x + 3.5f, SpawnValues.x - 3.5f), SpawnValues.y, SpawnValues.z);
						SpawnRotation = Quaternion.identity;
						Instantiate(ExpBox, SpawnPosition, SpawnRotation);
					}
				}
			}
			else if(score >= 100 && score < 130)
			{
				for(int i =0; i < 2; i++)
				{
					Vector3 SpawnPosition = new Vector3 (Random.Range (-SpawnValues.x + 3.5f, SpawnValues.x - 3.5f), SpawnValues.y, SpawnValues.z);
					Quaternion SpawnRotation = Quaternion.identity;
					Instantiate (Enemy_6, SpawnPosition, SpawnRotation);
					yield return new WaitForSeconds(0.8f);
					if(dead)
					{
						break;
					}
					if(Random.Range(0, 100f) > 95)
					{
						SpawnPosition = new Vector3 (Random.Range (-SpawnValues.x + 3.5f, SpawnValues.x - 3.5f), SpawnValues.y, SpawnValues.z);
						SpawnRotation = Quaternion.identity;
						Instantiate(lifeBox, SpawnPosition, SpawnRotation);
					}
					if(Random.Range(0, 100f) > 95)
					{
						SpawnPosition = new Vector3 (Random.Range (-SpawnValues.x + 3.5f, SpawnValues.x - 3.5f), SpawnValues.y, SpawnValues.z);
						SpawnRotation = Quaternion.identity;
						Instantiate(ExpBox, SpawnPosition, SpawnRotation);
					}
				}
			}
			else if(score >= 130 && score < 150 && !boss)
			{
				Vector3 SpawnPosition = new Vector3 (0f, SpawnValues.y - 5.0f, SpawnValues.z);
				Quaternion SpawnRotation = Quaternion.identity;
				Instantiate (Enemy_Boss, SpawnPosition, SpawnRotation);
				boss = true;
			}
			else if(boss)
			{
				for(int i =0; i < counter; i++)
				{
					Vector3 SpawnPosition = new Vector3 (Random.Range (-SpawnValues.x, SpawnValues.x), SpawnValues.y, SpawnValues.z);
					Quaternion SpawnRotation = Quaternion.identity;
					Instantiate (Enemy_1, SpawnPosition, SpawnRotation);
					yield return new WaitForSeconds(wait);
					if(dead)
					{
						break;
					}
					if(Random.Range(0, 100f) > 95)
					{
						SpawnPosition = new Vector3 (Random.Range (-SpawnValues.x + 3.5f, SpawnValues.x - 3.5f), SpawnValues.y, SpawnValues.z);
						SpawnRotation = Quaternion.identity;
						Instantiate(lifeBox, SpawnPosition, SpawnRotation);
					}
					if(Random.Range(0, 100f) > 95)
					{
						SpawnPosition = new Vector3 (Random.Range (-SpawnValues.x + 3.5f, SpawnValues.x - 3.5f), SpawnValues.y, SpawnValues.z);
						SpawnRotation = Quaternion.identity;
						Instantiate(ExpBox, SpawnPosition, SpawnRotation);
					}
				}
				counter++;
				if(wait > 0.01f)
				{
					wait -= 0.01f;
				}
			}
			if(dead)
			{
				break;
			}
			yield return new WaitForSeconds(WaitWaves);
		}
	}

	public void AddScore(int add)
	{
		score +=  add;
		UpdateScore ();
	}

	void UpdateScore()
	{
		Score.text = "Scores: " + score;
	}
	public void Death()
	{
		GameEnd.text = "GAME OVER!";
		GameEnd2.text = "PRESS O TO RESTART";
		dead = true;
	}
	public void GameOver()
	{
		life--;
		if (life == 0 && Relife == 0) 
		{
			Death();
		}
		else if(Relife > 0)
		{
			updateRespawn();
		}
	}
	public void addLife()
	{
		Relife++;
		updateLife ();
	}
	private void updateLife()
	{
		lifeNum.text = "Team Life: " + Relife;
	}
	private void updateRespawn()
	{
		RespawnText.text = "Press R to respawn!";
	}
	public void updateBomb()
	{
		Abomb.text = "ABOMB(PRESS P): " + bombNum;
	}
	public void addBomb()
	{
		bombNum++;
		updateBomb ();
	}
	void DestroyAllGameObjects()
	{
		GameObject[] GameObjects = GameObject.FindGameObjectsWithTag("Enemy")as GameObject[];
		
		for (int i = 0; i < GameObjects.Length; i++)
		{
			GameObject EnemyTemp = GameObjects[i];
			Transform EnemyT = EnemyTemp.GetComponent <Transform>();
			Instantiate(Explosion, EnemyT.position, Quaternion.identity);
			Destroy(GameObjects[i]);
		}
	}
}















