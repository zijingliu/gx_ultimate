﻿using UnityEngine;
using System.Collections;

public class Enemy6movement : MonoBehaviour {

	public float VerticalMovement;
	
	void Update()
	{
		Vector3 movement = new Vector3(8.0f * Mathf.Sin(2.5f * Time.time), -VerticalMovement, 0f);
		GetComponent<Rigidbody2D>().transform.position += movement * Time.deltaTime;
	}
}
