﻿using UnityEngine;
using System.Collections;

public class Enemy5movement : MonoBehaviour {

	public GameObject bullet;
	public GameObject Position;
	public float FireSpeed;

	float NextFire = 0.0f;
	
	void Update()
	{
		if (Time.time > NextFire) 
		{
			NextFire = Time.time + (1 / FireSpeed);
			Instantiate (bullet, Position.transform.position, Position.transform.rotation);
		}
	}
}
