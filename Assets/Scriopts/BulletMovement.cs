﻿using UnityEngine;
using System.Collections;

public class BulletMovement : MonoBehaviour 
{
	public float Speed;

	void FixedUpdate()
	{
		GetComponent<Rigidbody2D>().velocity = new Vector2(0.0f, Speed);
	}
}
