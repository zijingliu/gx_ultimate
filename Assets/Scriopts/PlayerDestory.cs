﻿using UnityEngine;
using System.Collections;

public class PlayerDestory : MonoBehaviour
{
	public GameObject Explosion;

	private GameController gameController;
	
	void Start ()
	{
		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		gameController = gameControllerObject.GetComponent <GameController>();
	}
	
	void OnTriggerEnter2D(Collider2D other) 
	{
		if(other.tag == "Player")
		{
			Destroy (other.gameObject);
			Destroy (gameObject);
			Instantiate(Explosion, other.transform.position, other.transform.rotation);
			gameController.Death();
		}

	}
}
