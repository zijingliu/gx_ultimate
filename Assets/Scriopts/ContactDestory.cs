﻿using UnityEngine;
using System.Collections;

public class ContactDestory : MonoBehaviour
{
	public GameObject Explosion;
	public GameObject Explosion_Player;
	public int ScoreValue;

	private GameController gameController;
	
	void Start ()
	{
		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		gameController = gameControllerObject.GetComponent <GameController>();
	}

	void OnTriggerEnter2D(Collider2D other) 
	{
		if (other.tag == "Boundary") 
		{
			return;
		} 
		if(other.tag == "Item" || other.tag == "Exp"|| other.tag == "Bomb")
		{
			return;
		}
		if(other.tag == "EnemyBullet")
		{
			return;
		}
		if(other.tag == "Boss")
		{
			return;
		}

		Instantiate(Explosion, transform.position, transform.rotation);


		if(other.tag == "Player")
		{
			Instantiate(Explosion_Player, other.transform.position, other.transform.rotation);
			gameController.GameOver();
		}

		gameController.AddScore(ScoreValue);
		Destroy (other.gameObject);
		Destroy (gameObject);
	}
}