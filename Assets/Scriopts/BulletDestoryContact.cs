﻿using UnityEngine;
using System.Collections;

public class BulletDestoryContact: MonoBehaviour 
{
	public GameObject Explosion_Player;
	private GameController gameController;
	
	void Start ()
	{
		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		gameController = gameControllerObject.GetComponent <GameController>();
	}
	
	void OnTriggerEnter2D(Collider2D other) 
	{
		if (other.tag == "Boundary") 
		{
			return;
		}

		if (other.tag == "Enemy") 
		{
			return;
		}
		if(other.tag == "Item" || other.tag == "Exp"|| other.tag == "Bomb")
		{
			return;
		}
		
		if(other.tag == "Player")
		{
			Instantiate(Explosion_Player, other.transform.position, other.transform.rotation);
			gameController.GameOver();
			Destroy (other.gameObject);
			Destroy (gameObject);
		}
	}	
}
